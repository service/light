package light.provider

import light.auto
import light.bean.annotation.Bean
import light.annotation.CompileStatic
import std.collection.HashMap
import yaml
import std.collection.ArrayList
import light.common.GetAllFiles
import std.fs.{FileInfo, Directory, File, OpenMode, Path}
import std.io.readToEnd

@When[os == "Windows"]
public var configPath = "\\config"

@When[os != "Windows"]
public var configPath = "/config"

/**
 * 配置文件提供者
 * 有限支持值注入 (字符串, 布尔值, 整数, 浮点数, 空值, HashMap<String, Any>, ArrayList<Any>)
*/
@Bean["config"]
public class ConfigProvider <: auto.ProviderAny {
    // 这里静态编译文件, 可以运行, 但是一直标红
    // @CompileStatic["/config"]
    // var files = HashMap<String, String>()
    
    /**
     * 所有配置, 每个文件对应一组
    */
    var all = HashMap<String, yaml.Value>()

    public init() {
        let files = GetAllFiles(configPath)
        for (d in files) {
            let file = File(d.path, OpenMode.Read)
            let fileText: Array<Byte> = readToEnd(file)
            file.close()
            let fileName = file.info.path.fileNameWithoutExtension
            this.all[fileName] = yaml.parse(fileText)
        }
    }

    public func getAny(alias: String): Any {
        let arr = alias.split(".")
        if (!this.all.contains(arr[0])) {
            throw Exception("配置文件 ${arr[0]}.yaml 不存在")
        }
        let file = this.all[arr[0]]
        if (arr.size == 1) {
            return file
        }

        var subAlias = arr[1]
        if (arr.size >= 2) {
            for (i in 2..arr.size) {
                subAlias = subAlias + "." + arr[i]
            }
        }

        return file.get(subAlias)
    }
}