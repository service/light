package light.log

import log as stdLog
import std.io.{OutputStream, ByteBuffer, BufferedOutputStream}
import std.console.Console
import std.fs.*
import std.collection.{ArrayList, Map, HashMap}
import std.collection.concurrent.NonBlockingQueue
import std.sync.AtomicBool
import std.time.DateTime
import log.{LogValue, LogWriter, Logger, Attr, LogRecord, LogLevel, setGlobalLogger, getGlobalLogger}

public var defaultLevel = LogLevel.TRACE
public var log: Logger = TextLogger(Console.stdOut, defaultLevel)

public func setDefaultLog(l: Logger) {
    log = l
    setGlobalLogger(log)
}

public func getDefaultLog() {
    getGlobalLogger()
}

open public class TextLogger <: Logger {
    let w: TextLogWriter

    let _closed = AtomicBool(false)

    protected let bo: BufferedOutputStream<OutputStream>
    let _attrs = ArrayList<Attr>()
    var _level = defaultLevel
    public init(output: OutputStream, level: LogLevel) {
        this._level = level
        this.bo = BufferedOutputStream<OutputStream>(output)
        this.w = TextLogWriter(this.bo)
    }

    public mut prop level: LogLevel {
        get() {
            this._level
        }
        set(v) {
            this._level = v
        }
    }
    public func withAttrs(attrs: Array<Attr>): Logger {
        if (attrs.size > 0) {
            let nl = TextLogger(w.out, this.level)
            nl._attrs.add(all: attrs)
            return nl
        }
        return this
    }

    // log
    public func log(level: LogLevel, message: String, attrs: Array<Attr>): Unit {
        if (this.enabled(level)) {
            let record: LogRecord = LogRecord(DateTime.now(), level, message, attrs)
            this.log(record)
        }
    }
    // lazy
    public func log(level: LogLevel, message: () -> String, attrs: Array<Attr>): Unit {
        if (this.enabled(level)) {
            let record: LogRecord = LogRecord(DateTime.now(), level, message(), attrs)
            this.log(record)
        }
    }

    open public func log(record: LogRecord): Unit {
        let colorStart: String
        let colorEnd: String

        var out = ""
        
        match (record.level.name) {
            case "DEBUG" =>
                colorStart = "\u{1b}[0;37m"
                colorEnd = "\u{1b}[0m"
            case "INFO" =>
                colorStart = "\u{1b}[0;32m"
                colorEnd = "\u{1b}[0m"
            case "WARN" =>
                colorStart = "\u{1b}[1;33m"
                colorEnd = "\u{1b}[0m"
            case "FATAL" =>
                colorStart = "\u{1b}[0;31m"
                colorEnd = "\u{1b}[0m"
            case "ERROR" =>
                colorStart = "\u{1b}[0;31m"
                colorEnd = "\u{1b}[0m"
            case _ => // 默认 TRACE
                colorStart = "\u{1b}[0;90m"
                colorEnd = "\u{1b}[0m"
        }
        out += colorStart
        out += "[" + record.time.format("yyyy-MM-dd HH:mm:ss.SSS").toString() + " " + record.level.toString() + "] "
        out += record.message + " "

        // write attrs
        for (i in 0..record.attrs.size) {
            let attr = record.attrs[i]
            out += attr[0]
            let ww = ToStringWriter()
            attr[1].writeTo(ww)
            out += ww.toString()
            if (i < record.attrs.size - 1) {
                out += " "
            }
        }
        this.w.writeString(out + colorEnd + "\n")

        this.bo.flush()
    }
    public func isClosed(): Bool {
        this._closed.load()
    }
    public func close(): Unit {
        if (isClosed()) {
            return
        }
        this._closed.store(true)
    }
}

class TextLogWriter <: LogWriter {
    var out: OutputStream
    init(out: OutputStream) {
        this.out = out
    }
    public func writeNone(): Unit {
        this.out.write("None".toArray())
    }
    public func writeInt(v: Int64): Unit {
        this.out.write(v.toString().toArray())
    }
    public func writeUInt(v: UInt64): Unit {
        this.out.write(v.toString().toArray())
    }
    public func writeBool(v: Bool): Unit {
        this.out.write(v.toString().toArray())
    }
    public func writeFloat(v: Float64): Unit {
        this.out.write(v.toString().toArray())
    }
    public func writeString(v: String): Unit {
        this.out.write(v.toArray())
    }
    public func writeDateTime(v: DateTime): Unit {
        this.out.write(v.toString().toArray())
    }
    public func writeDuration(v: Duration): Unit {
        this.out.write(v.toString().toArray())
    }
    public func writeKey(v: String): Unit {
        this.out.write(v.toString().toArray())
        this.out.write("=".toArray())
    }
    public func writeValue(v: LogValue): Unit {
        match (v) {
            case vv: String =>
                this.out.write("\"".toArray())
                this.out.write(vv.toArray())
                this.out.write("\"".toArray())
            case vv: ToString =>
                this.out.write("\"".toArray())
                this.out.write(vv.toString().toArray())
                this.out.write("\"".toArray())
            case _ =>
                this.out.write("\"".toArray())
                v.writeTo(this)
                this.out.write("\"".toArray())
        }
    }
    public func startArray(): Unit {
        this.out.write("[".toArray())
    }
    public func endArray(): Unit {
        this.out.write("]".toArray())
    }
    public func startObject(): Unit {
        this.out.write("{".toArray())
    }
    public func endObject(): Unit {
        this.out.write("}".toArray())
    }

    public func writeException(v: Exception): Unit {
        Console.stdErr.write(v.toString())
    }
}

class ToStringWriter <: LogWriter {
    private var out = ""

    public func toString(): String {
        return out
    }

    public func writeNone(): Unit {
        this.out += "None"
    }
    public func writeInt(v: Int64): Unit {
        this.out += v.toString()
    }
    public func writeUInt(v: UInt64): Unit {
        this.out += v.toString()
    }
    public func writeBool(v: Bool): Unit {
        this.out += v.toString()
    }
    public func writeFloat(v: Float64): Unit {
       this.out += v.toString()
    }
    public func writeString(v: String): Unit {
        this.out += v
    }
    public func writeDateTime(v: DateTime): Unit {
        this.out += v.toString()
    }
    public func writeDuration(v: Duration): Unit {
        this.out += v.toString()
    }
    public func writeKey(v: String): Unit {
        this.out += v.toString() + "="
    }
    public func writeValue(v: LogValue): Unit {
        match (v) {
            case vv: String =>
                this.out += "\""
                this.out += vv
                this.out += "\""

            case vv: ToString =>
                this.out += "\""
                this.out += vv.toString()
                this.out += "\""
            case _ =>
                this.out += "\""
                let w = ToStringWriter()
                v.writeTo(w)
                this.out += w.toString()
                this.out += "\""
        }
    }
    public func startArray(): Unit {
        this.out += "["
    }
    public func endArray(): Unit {
        this.out += "]"
    }
    public func startObject(): Unit {
        this.out += "{"
    }
    public func endObject(): Unit {
        this.out += "}"
    }

    public func writeException(v: Exception): Unit {
        Console.stdErr.write(v.toString())
    }
}